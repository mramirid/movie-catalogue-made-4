package com.mramirid.moviecatalogue.activity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mramirid.moviecatalogue.R;
import com.mramirid.moviecatalogue.adapter.ViewPagerAdapter;
import com.mramirid.moviecatalogue.database.AppDbHelper;
import com.mramirid.moviecatalogue.fragment.ItemsFragment;

public class MainActivity extends AppCompatActivity {

	public static final String MOVIES = "movie";
	public static final String TV_SHOWS = "tv";

	public static AppDbHelper appDbHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		appDbHelper = AppDbHelper.getInstance(getApplicationContext());
		appDbHelper.open();

		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		ViewPager viewPager = findViewById(R.id.viewpager);
		setupViewPager(viewPager);

		TabLayout tabLayout = findViewById(R.id.tabs);
		tabLayout.setupWithViewPager(viewPager);
	}

	private void setupViewPager(ViewPager viewPager) {
		ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
		adapter.addFragment(new ItemsFragment(MOVIES), getResources().getString(R.string.movies));
		adapter.addFragment(new ItemsFragment(TV_SHOWS), getResources().getString(R.string.tv_shows));
		viewPager.setAdapter(adapter);
		viewPager.setOffscreenPageLimit(2);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_change_settings:
				Intent moveToLanguageSettings = new Intent(Settings.ACTION_LOCALE_SETTINGS);
				startActivity(moveToLanguageSettings);
				break;
			case R.id.action_show_favorites:
				Intent moveToFavorites = new Intent(this, FavoritesActivity.class);
				startActivity(moveToFavorites);
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		appDbHelper.close();
	}
}
