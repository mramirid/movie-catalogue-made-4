package com.mramirid.moviecatalogue.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.mramirid.moviecatalogue.R;
import com.mramirid.moviecatalogue.database.FavoritesDbContract;
import com.mramirid.moviecatalogue.model.Item;

public class ItemDetailActivity extends AppCompatActivity {

	private ImageView imgCoverDetail, imgPhotoDetail;
	private TextView tvNameDetail, tvGenresDetail, tvYearDetail, tvLang, tvDescriptionDetail;
	private RatingBar ratingBar;
	private Item item;

	public static final String ITEM_EXTRA = "item_extra";
	public static final int REMOVE_RESULT_CODE = 101;

	private boolean isRemoved = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_item_detail);

		findViews();

		item = getIntent().getParcelableExtra(ITEM_EXTRA);
		final ToggleButton favoriteButton = findViewById(R.id.fav_button);
		boolean hasInserted = MainActivity.appDbHelper.isInDatabase(item.getId(), FavoritesDbContract.TABLE_NAME);

		if (hasInserted) {
			favoriteButton.setChecked(true);
			favoriteButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_red_24dp));
		} else {
			favoriteButton.setChecked(false);
			favoriteButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_grey_24dp));
		}

		favoriteButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
				if (b) {
					favoriteButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_red_24dp));
					MainActivity.appDbHelper.insertFavoriteItem(item);
					isRemoved = false;
					Toast.makeText(ItemDetailActivity.this, R.string.added_to_favorites, Toast.LENGTH_SHORT).show();
				}
				else {
					favoriteButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_grey_24dp));
					MainActivity.appDbHelper.deleteItem(item.getId());
					isRemoved = true;
					Toast.makeText(ItemDetailActivity.this, R.string.removed_from_favorites, Toast.LENGTH_SHORT).show();
				}
			}
		});

		parseGenres();
		bindViews();
	}

	@Override
	public void finish() {
		if (isRemoved) {
			Intent resultIntent = new Intent();
			setResult(REMOVE_RESULT_CODE, resultIntent);
		}
		super.finish();
	}

	private void findViews() {
		imgCoverDetail = findViewById(R.id.img_cover_detail);
		imgPhotoDetail = findViewById(R.id.img_photo_detail);
		tvNameDetail = findViewById(R.id.tv_name_detail);
		ratingBar = findViewById(R.id.rb_star);
		tvGenresDetail = findViewById(R.id.tv_genres_detail);
		tvYearDetail = findViewById(R.id.tv_year_detail);
		tvLang = findViewById(R.id.tv_lang_detail);
		tvDescriptionDetail = findViewById(R.id.tv_description_detail);
	}

	private void bindViews() {
		Glide.with(this).load(this.item.getPoster()).centerCrop().into(imgCoverDetail);
		Glide.with(this)
				.load(this.item.getPoster())
				.apply(new RequestOptions().transform(new RoundedCorners(40)))
				.into(imgPhotoDetail);
		tvNameDetail.setText(this.item.getName());
		ratingBar.setRating(this.item.getRating());
		tvGenresDetail.setText(parseGenres());
		tvYearDetail.setText(this.item.getYear());
		tvLang.setText(this.item.getLanguage());
		tvDescriptionDetail.setText(this.item.getDescription());
	}

	public String parseGenres() {
		String[] genresId = item.getGenres().split("_");

		StringBuilder stringBuilder = new StringBuilder();
		for (String genreId : genresId) {
			String genreName = MainActivity.appDbHelper.getGenreById(Integer.valueOf(genreId));
			stringBuilder.append(genreName).append(", ");
		}

		String genres = stringBuilder.toString();
		return genres.substring(0, genres.length() - 2);	// Hilangkan tanda koma & spasi di akhir String
	}
}
