package com.mramirid.moviecatalogue.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.mramirid.moviecatalogue.model.Genre;
import com.mramirid.moviecatalogue.model.Item;

import java.util.ArrayList;

public class AppDbHelper {

	private static final String FAVORITES_TABLE = FavoritesDbContract.TABLE_NAME;
	private static final String GENRES_TABLE = GenresDbContract.TABLE_NAME;
	private static DatabaseHelper databaseHelper;
	private static AppDbHelper INSTANCE;

	private static SQLiteDatabase database;

	private AppDbHelper(Context context) {
		databaseHelper = new DatabaseHelper(context);
	}

	public static AppDbHelper getInstance(Context context) {
		if (INSTANCE == null) {
			synchronized (SQLiteOpenHelper.class) {
				if (INSTANCE == null)
					INSTANCE = new AppDbHelper(context);
			}
		}
		return INSTANCE;
	}

	public void open() throws SQLException {
		database = databaseHelper.getWritableDatabase();
	}

	public void close() {
		databaseHelper.close();

		if (database.isOpen())
			database.close();
	}

	public void beginTransaction() {
		database.beginTransaction();
	}

	public void setTransactionSuccess() {
		database.setTransactionSuccessful();
	}

	public void endTransaction() {
		database.endTransaction();
	}

	public void insertTransaction(Genre genre) {
		String sql = "INSERT INTO " + GENRES_TABLE + " (" +
				GenresDbContract.GenresColumns._ID + ", " +
				GenresDbContract.GenresColumns.NAME + ") VALUES (?, ?)";
		SQLiteStatement sqLiteStatement = database.compileStatement(sql);
		sqLiteStatement.bindString(1, String.valueOf(genre.getId()));
		sqLiteStatement.bindString(2, genre.getName());
		sqLiteStatement.execute();
		sqLiteStatement.clearBindings();
	}

	public String getGenreById(int id) {
		Cursor cursor = database.query(
				GENRES_TABLE,
				new String[] {GenresDbContract.GenresColumns.NAME},
				GenresDbContract.GenresColumns._ID + " = " + id,
				null,
				null,
				null,
				null,
				null
		);

		cursor.moveToFirst();
		String selectedGenreName = null;
		if (cursor.getCount() > 0)
			selectedGenreName = cursor.getString(cursor.getColumnIndexOrThrow(GenresDbContract.GenresColumns.NAME));
		cursor.close();
		return selectedGenreName;
	}

	public ArrayList<Item> getAllFavorites(String itemType) {
		ArrayList<Item> items = new ArrayList<>();
		Cursor cursor = database.query(
				FAVORITES_TABLE,
				null,
				FavoritesDbContract.FavoritesColumns.ITEM_TYPE + " = \'" + itemType + "\'",
				null,
				null,
				null,
				FavoritesDbContract.FavoritesColumns._ID + " ASC",
				null
		);

		cursor.moveToFirst();
		if (cursor.getCount() > 0) {
			Item item;
			do {
				item = new Item();
				item.setId(cursor.getInt(cursor.getColumnIndexOrThrow(FavoritesDbContract.FavoritesColumns._ID)));
				item.setItemType(cursor.getString(cursor.getColumnIndexOrThrow(FavoritesDbContract.FavoritesColumns.ITEM_TYPE)));
				item.setPoster(cursor.getString(cursor.getColumnIndexOrThrow(FavoritesDbContract.FavoritesColumns.POSTER)));
				item.setName(cursor.getString(cursor.getColumnIndexOrThrow(FavoritesDbContract.FavoritesColumns.NAME)));
				item.setGenres(cursor.getString(cursor.getColumnIndexOrThrow(FavoritesDbContract.FavoritesColumns.GENRES)));
				item.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(FavoritesDbContract.FavoritesColumns.DESCRIPTION)));
				item.setYear(cursor.getString(cursor.getColumnIndexOrThrow(FavoritesDbContract.FavoritesColumns.YEAR)));
				item.setLanguage(cursor.getString(cursor.getColumnIndexOrThrow(FavoritesDbContract.FavoritesColumns.LANGUAGE)));
				item.setRating(cursor.getFloat(cursor.getColumnIndexOrThrow(FavoritesDbContract.FavoritesColumns.RATING)));

				items.add(item);
				cursor.moveToNext();
			} while (!cursor.isAfterLast());
		}
		cursor.close();
		return items;
	}

	public boolean isInDatabase(int id, String tableName) {
		boolean hasInserted = false;
		Cursor cursor = database.query(
				tableName,
				new String[] {FavoritesDbContract.FavoritesColumns._ID},
				FavoritesDbContract.FavoritesColumns._ID + " = " + id,
				null,
				null,
				null,
				null,
				null
		);

		cursor.moveToFirst();
		if (cursor.getCount() > 0) {
			int selectedId = cursor.getInt(cursor.getColumnIndexOrThrow(FavoritesDbContract.FavoritesColumns._ID));
			if (selectedId == id)
				hasInserted = true;
		}
		cursor.close();
		return hasInserted;
	}

	public void insertFavoriteItem(Item item) {
		ContentValues values = new ContentValues();
		values.put(FavoritesDbContract.FavoritesColumns._ID, item.getId());
		values.put(FavoritesDbContract.FavoritesColumns.ITEM_TYPE, item.getItemType());
		values.put(FavoritesDbContract.FavoritesColumns.POSTER, item.getPoster());
		values.put(FavoritesDbContract.FavoritesColumns.NAME, item.getName());
		values.put(FavoritesDbContract.FavoritesColumns.GENRES, item.getGenres());
		values.put(FavoritesDbContract.FavoritesColumns.DESCRIPTION, item.getDescription());
		values.put(FavoritesDbContract.FavoritesColumns.YEAR, item.getYear());
		values.put(FavoritesDbContract.FavoritesColumns.LANGUAGE, item.getLanguage());
		values.put(FavoritesDbContract.FavoritesColumns.RATING, item.getRating());
		database.insert(FAVORITES_TABLE, null, values);
	}

	public void deleteItem(int id) {
		database.delete(FavoritesDbContract.TABLE_NAME, FavoritesDbContract.FavoritesColumns._ID + "= \'" + id + "\'", null);
	}
}
