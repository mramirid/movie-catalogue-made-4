package com.mramirid.moviecatalogue.database;

import android.provider.BaseColumns;

public class GenresDbContract {

	public static String TABLE_NAME = "genres";

	static final class GenresColumns implements BaseColumns {
		static final String NAME = "name";
	}
}
