package com.mramirid.moviecatalogue.database;

import android.provider.BaseColumns;

public class FavoritesDbContract {

	public static String TABLE_NAME = "favorites";

	static final class FavoritesColumns implements BaseColumns {
		static final String ITEM_TYPE = "item_type";
		static final String POSTER = "poster";
		static final String NAME = "name";
		static final String GENRES = "genres";
		static final String DESCRIPTION = "description";
		static final String YEAR = "year";
		static final String LANGUAGE = "language";
		static final String RATING = "rating";
	}
}
