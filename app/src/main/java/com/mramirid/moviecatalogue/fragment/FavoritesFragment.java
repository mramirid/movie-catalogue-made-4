package com.mramirid.moviecatalogue.fragment;


import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mramirid.moviecatalogue.R;
import com.mramirid.moviecatalogue.activity.ItemDetailActivity;
import com.mramirid.moviecatalogue.adapter.ItemsAdapter;
import com.mramirid.moviecatalogue.model.Item;
import com.mramirid.moviecatalogue.viewmodel.FavoritesViewModel;

import java.util.ArrayList;

public class FavoritesFragment extends Fragment {

	private String itemType;
	private ItemsAdapter itemsAdapter;
	private FavoritesViewModel favoritesViewModel;
	private ProgressBar progressBar;

	private static final int REQUEST_CODE = 100;

	private int selectedItemPosition;

	public FavoritesFragment() {
		// Required empty public constructor
	}

	public FavoritesFragment(String itemType) {
		this.itemType = itemType;
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_list_items, container, false);

		itemsAdapter = new ItemsAdapter(getContext());
		itemsAdapter.notifyDataSetChanged();

		RecyclerView recyclerView = view.findViewById(R.id.rv_items);
		recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		recyclerView.setAdapter(itemsAdapter);

		int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.default_margin);
		recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

		progressBar = view.findViewById(R.id.progress_bar);

		favoritesViewModel = ViewModelProviders.of(this).get(FavoritesViewModel.class);
		favoritesViewModel.getFavoritesLiveData().observe(this, getFavorite);
		favoritesViewModel.getRequestStatus().observe(this, hasLoaded);
		favoritesViewModel.getHasRemovedLiveData().observe(this, getRemoved);

		setItems(itemType);

		itemsAdapter.setOnItemClickCallback(new ItemsAdapter.OnItemClickCallback() {
			@Override
			public void onItemClicked(Item item, ItemsAdapter.ItemViewHolder holder, int position) {
				selectedItemPosition = position;

				// Set transition
				Pair[] pairs = new Pair[3];
				pairs[0] = new Pair<View, String>(holder.imgPoster, "posterTransition");
				pairs[1] = new Pair<View, String>(holder.tvName, "nameTransition");
				pairs[2] = new Pair<View, String>(holder.ratingBar, "ratingBarTransition");
				ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), pairs);

				Intent intent = new Intent(getActivity(), ItemDetailActivity.class);
				intent.putExtra(ItemDetailActivity.ITEM_EXTRA, item);
				startActivityForResult(intent, REQUEST_CODE, options.toBundle());
			}
		});

		return view;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_CODE) {
			if (resultCode == ItemDetailActivity.REMOVE_RESULT_CODE)
				favoritesViewModel.deleteFavorite(selectedItemPosition);
		}
	}

	private void setItems(String itemType) {
		favoritesViewModel.setupFavoritesFromDb(itemType);
		showLoading(true);
	}

	private void showLoading(boolean state) {
		if (state)
			progressBar.setVisibility(View.VISIBLE);
		else
			progressBar.setVisibility(View.GONE);
	}

	private Observer<ArrayList<Item>> getFavorite = new Observer<ArrayList<Item>>() {
		@Override
		public void onChanged(ArrayList<Item> items) {
			if (items != null) {
				itemsAdapter.setData(items);
				showLoading(false);
			}
		}
	};

	private Observer<Boolean> hasLoaded = new Observer<Boolean>() {
		@Override
		public void onChanged(Boolean aBoolean) {
			if (!aBoolean) {
				Toast.makeText(getActivity(), R.string.request_items_failed, Toast.LENGTH_SHORT).show();
				showLoading(false);
			}
		}
	};

	private Observer<Boolean> getRemoved = new Observer<Boolean>() {
		@Override
		public void onChanged(Boolean aBoolean) {
			if (aBoolean) {
				itemsAdapter.itemsList.remove(selectedItemPosition);
				itemsAdapter.notifyItemRemoved(selectedItemPosition);
				int sizeOfNewItemsList = itemsAdapter.itemsList.size();
				itemsAdapter.notifyItemRangeChanged(selectedItemPosition, sizeOfNewItemsList);
			}
		}
	};
}
