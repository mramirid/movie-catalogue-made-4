package com.mramirid.moviecatalogue.fragment;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mramirid.moviecatalogue.R;
import com.mramirid.moviecatalogue.activity.ItemDetailActivity;
import com.mramirid.moviecatalogue.activity.MainActivity;
import com.mramirid.moviecatalogue.adapter.ItemsAdapter;
import com.mramirid.moviecatalogue.database.GenresDbContract;
import com.mramirid.moviecatalogue.model.Genre;
import com.mramirid.moviecatalogue.model.Item;
import com.mramirid.moviecatalogue.preference.AppPreference;
import com.mramirid.moviecatalogue.viewmodel.ItemsViewModel;

import java.util.ArrayList;
import java.util.Objects;

public class ItemsFragment extends Fragment {

	private String itemType;
	private ItemsAdapter itemsAdapter;
	private ItemsViewModel itemsViewModel;
	private ProgressBar progressBar;

	private AppPreference appPreference;

	public ItemsFragment() {
		// Required empty public constructor
	}

	public ItemsFragment(String itemType) {
		this.itemType = itemType;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		appPreference = new AppPreference(Objects.requireNonNull(getContext()));
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_list_items, container, false);

		itemsAdapter = new ItemsAdapter(getContext());
		itemsAdapter.notifyDataSetChanged();

		RecyclerView recyclerView = view.findViewById(R.id.rv_items);
		recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		recyclerView.setAdapter(itemsAdapter);

		int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.default_margin);
		recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

		progressBar = view.findViewById(R.id.progress_bar);

		itemsViewModel = ViewModelProviders.of(this).get(ItemsViewModel.class);
		itemsViewModel.getItemsLiveData().observe(this, getItems);
		itemsViewModel.getGenresLiveData().observe(this, getGenres);
		itemsViewModel.getItemsRequestStatus().observe(this, itemsReceived);
		itemsViewModel.getGenresRequestStatus().observe(this, genresReceived);

		setItems(itemType);

		itemsAdapter.setOnItemClickCallback(new ItemsAdapter.OnItemClickCallback() {
			@Override
			public void onItemClicked(Item item, ItemsAdapter.ItemViewHolder holder, int position) {
				// Set transition
				Pair[] pairs = new Pair[3];
				pairs[0] = new Pair<View, String>(holder.imgPoster, "posterTransition");
				pairs[1] = new Pair<View, String>(holder.tvName, "nameTransition");
				pairs[2] = new Pair<View, String>(holder.ratingBar, "ratingBarTransition");
				ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), pairs);

				Intent intent = new Intent(getActivity(), ItemDetailActivity.class);
				intent.putExtra(ItemDetailActivity.ITEM_EXTRA, item);
				startActivity(intent, options.toBundle());
			}
		});

		return view;
	}

	private void setItems(String itemType) {
		Boolean firstRun = appPreference.getAppFirstRun();

		if (firstRun) {
			itemsViewModel.requestGenresFromApi(itemType);
			itemsViewModel.requestItemsListFromApi(itemType);
		}
		else {
			itemsViewModel.requestItemsListFromApi(itemType);
		}

		showLoading(true);
	}

	private void showLoading(boolean state) {
		if (state)
			progressBar.setVisibility(View.VISIBLE);
		else
			progressBar.setVisibility(View.GONE);
	}

	private Observer<ArrayList<Item>> getItems = new Observer<ArrayList<Item>>() {
		@Override
		public void onChanged(ArrayList<Item> items) {
			if (items != null) {
				itemsAdapter.setData(items);
				showLoading(false);
			}
		}
	};

	private Observer<ArrayList<Genre>> getGenres = new Observer<ArrayList<Genre>>() {
		@Override
		public void onChanged(ArrayList<Genre> genres) {
			if (genres != null) {
				// Input ArrayList genres ke dalam database

				MainActivity.appDbHelper.beginTransaction();

				for (Genre genre : genres) {
					// Jika id sudah ada di database, maka abaikan
					boolean hasInserted = MainActivity.appDbHelper.isInDatabase(genre.getId(), GenresDbContract.TABLE_NAME);
					if (!hasInserted)
						MainActivity.appDbHelper.insertTransaction(genre);
				}

				MainActivity.appDbHelper.setTransactionSuccess();
				MainActivity.appDbHelper.endTransaction();
				appPreference.setAppFirstRun(false);
			}
		}
	};

	private Observer<Boolean> itemsReceived = new Observer<Boolean>() {
		@Override
		public void onChanged(Boolean aBoolean) {
			if (!aBoolean) {
				Toast.makeText(getActivity(), R.string.request_items_failed, Toast.LENGTH_SHORT).show();
				showLoading(false);
			}
		}
	};

	private Observer<Boolean> genresReceived = new Observer<Boolean>() {
		@Override
		public void onChanged(Boolean aBoolean) {
			if (!aBoolean) {
				Toast.makeText(getActivity(), R.string.request_genres_failed, Toast.LENGTH_SHORT).show();
				appPreference.setAppFirstRun(true);
			}
		}
	};
}
