package com.mramirid.moviecatalogue.viewmodel;

import com.mramirid.moviecatalogue.model.Item;

import java.util.ArrayList;

public interface LoadItemsFavoriteCallback {
	void postExecute(ArrayList<Item> items);
}
