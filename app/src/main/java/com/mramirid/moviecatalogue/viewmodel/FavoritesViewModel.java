package com.mramirid.moviecatalogue.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mramirid.moviecatalogue.model.Item;

import java.util.ArrayList;

public class FavoritesViewModel extends ViewModel implements LoadItemsFavoriteCallback {

	private final ArrayList<Item> favoritesList = new ArrayList<>();
	private MutableLiveData<ArrayList<Item>> favoritesLiveData = new MutableLiveData<>();
	private MutableLiveData<Boolean> hasLoadedLiveData = new MutableLiveData<>();
	private MutableLiveData<Boolean> hasRemovedLiveData = new MutableLiveData<>();

	public void setupFavoritesFromDb(String itemType) {
		LoadFavoritesAsync loadFavorites = new LoadFavoritesAsync(this, itemType);
		loadFavorites.execute();
	}

	@Override
	public void postExecute(ArrayList<Item> items) {
		favoritesList.addAll(items);
		favoritesLiveData.postValue(favoritesList);
		hasLoadedLiveData.postValue(true);
	}

	public void deleteFavorite(int position) {
		favoritesList.remove(position);
		hasRemovedLiveData.postValue(true);
	}

	public LiveData<ArrayList<Item>> getFavoritesLiveData() {
		return favoritesLiveData;
	}

	public LiveData<Boolean> getRequestStatus() {
		return hasLoadedLiveData;
	}

	public MutableLiveData<Boolean> getHasRemovedLiveData() {
		return hasRemovedLiveData;
	}
}
