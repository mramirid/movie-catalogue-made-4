package com.mramirid.moviecatalogue.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.mramirid.moviecatalogue.BuildConfig;
import com.mramirid.moviecatalogue.model.Genre;
import com.mramirid.moviecatalogue.model.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;

public class ItemsViewModel extends ViewModel {

	private MutableLiveData<ArrayList<Item>> itemsLiveData = new MutableLiveData<>();
	private MutableLiveData<Boolean> itemsReceivedLiveData = new MutableLiveData<>();
	private final ArrayList<Item> itemsList = new ArrayList<>();

	private MutableLiveData<ArrayList<Genre>> genresLiveData = new MutableLiveData<>();
	private MutableLiveData<Boolean> genresReceivedLiveData = new MutableLiveData<>();
	private ArrayList<Genre> genresList = new ArrayList<>();

	public void requestGenresFromApi(String itemType) {
		String url = "https://api.themoviedb.org/3/genre/" + itemType + "/list?api_key=" + BuildConfig.API_KEY + "&language=en-US";

		AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				try {
					String result = new String(responseBody);
					JSONObject responseObject = new JSONObject(result);
					JSONArray list = responseObject.getJSONArray("genres");

					for (int i = 0; i < list.length(); ++i) {
						JSONObject genreObject = list.getJSONObject(i);
						Genre genre = new Genre();
						genre.setId(genreObject.getInt("id"));
						genre.setName(genreObject.getString("name"));
						genresList.add(genre);
					}

					genresLiveData.postValue(genresList);
					genresReceivedLiveData.postValue(true);
				} catch (JSONException e) {
					Log.d("JSONException", Objects.requireNonNull(e.getMessage()));
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
				Log.d("onFailure", Objects.requireNonNull(error.getMessage()));
				genresReceivedLiveData.postValue(false);
			}
		});
	}

	public void requestItemsListFromApi(final String itemType) {
		AsyncHttpClient client = new AsyncHttpClient();
		String url = "https://api.themoviedb.org/3/discover/" + itemType + "?api_key=" + BuildConfig.API_KEY + "&language=en-US";

		client.get(url, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				try {
					String result = new String(responseBody);
					JSONObject responseObject = new JSONObject(result);
					JSONArray list = responseObject.getJSONArray("results");

					for (int i = 0; i < list.length(); ++i) {
						JSONObject itemData = list.getJSONObject(i);
						Item item = new Item(itemData, itemType);
						itemsList.add(item);
					}

					itemsLiveData.postValue(itemsList);
					itemsReceivedLiveData.postValue(true);
				} catch (JSONException e) {
					Log.d("JSONException", Objects.requireNonNull(e.getMessage()));
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
				Log.d("onFailure", Objects.requireNonNull(error.getMessage()));
				itemsReceivedLiveData.postValue(false);
			}
		});
	}

	public LiveData<ArrayList<Item>> getItemsLiveData() {
		return itemsLiveData;
	}

	public LiveData<ArrayList<Genre>> getGenresLiveData() {
		return genresLiveData;
	}

	public LiveData<Boolean> getGenresRequestStatus() {
		return genresReceivedLiveData;
	}

	public LiveData<Boolean> getItemsRequestStatus() {
		return itemsReceivedLiveData;
	}
}
