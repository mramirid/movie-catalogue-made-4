package com.mramirid.moviecatalogue.viewmodel;

import android.os.AsyncTask;

import com.mramirid.moviecatalogue.activity.MainActivity;
import com.mramirid.moviecatalogue.model.Item;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class LoadFavoritesAsync extends AsyncTask<Void, Void, ArrayList<Item>> {

	private String itemType;
	private final WeakReference<FavoritesViewModel> weakCallback;

	LoadFavoritesAsync(FavoritesViewModel callback, String itemType) {
		this.itemType = itemType;
		this.weakCallback = new WeakReference<>(callback);
	}

	@Override
	protected ArrayList<Item> doInBackground(Void... voids) {
		return MainActivity.appDbHelper.getAllFavorites(itemType);
	}

	@Override
	protected void onPostExecute(ArrayList<Item> items) {
		super.onPostExecute(items);
		weakCallback.get().postExecute(items);
	}
}
